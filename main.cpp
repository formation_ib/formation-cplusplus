#include <iostream>

//#include "bases.h"
#include "fonctions.h"
#include "pointeurs.h"
#include "objets.h"
#include "operateurs.h"
#include "heritage.h"
#include "exceptions.h"
#include "templates.h"
#include "stl.h"


using namespace std;


// fonction de depart
int main(){
    // typesEtTestBoucle();
    // parametres();
    // montreOuvrage("Le Mahabaratha");
    // variablesFonctions();
    // pointeursEtTableaux();
    // tableauxEtFonctions();
    // references();
    // basesDeLobjet();
    // objetsComplets();
    // formeCanonique();
    // visibiliteEtOperateurs();
    // heritageSimple();
    // exceptions();
    // templates();
    collections();
    ensemble();
    pointeurIntelligents();
}
