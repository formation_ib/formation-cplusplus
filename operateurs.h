#ifndef OPERATEURS_H_INCLUDED
#define OPERATEURS_H_INCLUDED

using namespace std;

void visibiliteEtOperateurs();

// -----------------
// |  Rayon        |
// -----------------
// | - nom         |
// | - ouvrages    |
// -----------------
// | + Rayon()     |
// | + Rayon(n, o) |
// -----------------
class Rayon {
public:
    Rayon();
    Rayon(const string);
    Rayon(const string, const unsigned);
    //~Rayon();
    void afficher() const;
    //Operateurs
    Rayon operator+(const Rayon &) const; // "const" a la fin indique que this va pas changer

    //Ex1: comparer les nom avec les operateurs
    bool operator==(const Rayon &) const;
    bool operator!=(const Rayon &) const;

    //Ex.2
    void operator ()() const;
private:
    string nom;
    unsigned ouvrages;

    friend void visibiliteEtOperateurs();


};


#endif // OPERATEURS_H_INCLUDED
