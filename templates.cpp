#include <iostream>
#include <vector>

#include "templates.h"

using namespace std;

//const int CTE = int(35);
template<typename T> const T PI = T(3.14159265352847);

template<typename T> void afficher(T x, T y) {
    cout << x << " et " << y << endl;
}

template<typename T> T getX() {
    cout << "x ?";
    T x;
    cin >> x;
    return x;
}

template<typename T> T somme(T x, T y, T z) {
    return x + y + z;
}

template<class T1, class T2> decltype(auto) mult(T1 a, T2 b) {
    return a*b;
}

template<class T1, class T2> auto mult2(T1 a, T2 b) -> decltype(a*b){
    return a*b;
}

template<typename T> struct Conteneur {
    T v;
    //Constructeur-->>
    Conteneur(){} // autre syntaxe Conteneur() : v() {}
    Conteneur(T arg): v(arg) {}

    //bool egalApproximatif(int n) {
    //    return (n>v-1) && (n<v+1);
    //}
    //transformer int n a un argument quelconde -->> transfomer en template de methode :
    template<typename T2> bool egalApproximatif(T2 n) {
        return (n>v-1) && (n<v+1);
    }
};

template<> struct Conteneur<string> {
    string v;
    Conteneur() : v("inconnue") {} // autre syntaxe Conteneur() : v() {}
    Conteneur(string arg): v(arg) {}
};

//specialitsation partielle:
//    template<typename T1, typename T2> struct S {.....
//    template<typename T1> struct S<T1,string> {.....
//    template<typename T2> struct S<int,T2> {.....
//    template<> struct S<int,double> {.....

namespace formation_ib {
    template<int V> unsigned multiplieur(unsigned x) { return x*V ;}
}


void templates() {
    cout << "Templates : " << endl;
    float pif = PI<float>;
    cout << "float: " << pif << endl;
    cout << "double: " << PI<double> << endl;
    cout << "unsigned: " << PI<unsigned> << endl;
 //   cout << "string: " << PI<string> << endl; interdit car la constante PI ne peut pas etre utiliser ici string(3.1415)
    afficher<int>(4,12);
    afficher("Ann","Bob");
    afficher<double>(6, 8.3);

    int v = getX<int>();
    cout << v << endl;

    cout << somme(string("a"), string("b"), string("c")) << " easy as " << somme(1,2,3) <<endl;

    cout << "Multiplication : " << mult(5,2) << " " << mult(5.2,2.6) << " "  << mult(5,2.2) << " "  << mult(5.7,2) << endl;
    cout << "Multiplication 2 : " << mult2(5,2) << " " << mult2(5.2,2.6) << " "  << mult2(5,2.2) << " "  << mult2(5.7,2) << endl;

    Conteneur<float> c1;
    c1.v = 2.3199f;
    Conteneur<float> c2(3.4f);
    cout << "Conteneurs : " << c1.v << " " << c2.v << " " << c2.egalApproximatif(4.5) <<  endl;
    Conteneur<string> c3;
    cout << "Conteneur 3 : " << c3.v << endl; // "Conteneur 3 : inconnue"

    cout << "Template sur entier : " << formation_ib::multiplieur<2+2>(5) << endl; //20










}



















