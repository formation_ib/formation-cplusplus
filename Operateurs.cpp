#include <iostream>

#include "operateurs.h"

using namespace std;


Rayon::Rayon() : Rayon("",0) {}
Rayon::Rayon(const string n, const unsigned o) : nom(n), ouvrages(o) {}

void Rayon::afficher () const {
    cout << nom << " (" << ouvrages << ")" << endl;
}

//Operateurs
// r1 + r2: this = &r1   et    scr2 = r2
Rayon Rayon::operator+(const Rayon & src2) const
{
    Rayon res (this->nom+"/"+src2.nom, this->ouvrages+src2.ouvrages);

    return res;
}

//Ex1: comparer les nom avec les operateurs ==
bool Rayon::operator==(const Rayon & src3) const
{
    return this->nom==src3.nom;
}
//Ex1: comparer les nom avec les operateurs !=
bool Rayon::operator!=(const Rayon & src3) const
{
    return !(*this==src3);
}

void Rayon::operator ()() const{
    afficher();
}


void visibiliteEtOperateurs(){
    cout << " Visibilite et Operateurs " << endl;
//    Rayon r1;
//    r1.nom = "Histoire";
//    r1.ouvrages = 4982;
//    cout << r1.nom << endl;
    Rayon r1 { "Geographie", 8711 };
    r1.ouvrages += 62; // ok : dans une amie de Rayon
    r1.afficher();

    //Operateurs
    // naif :  Rayon r3 = r1.operatuers+(r2);
    Rayon r2 { "Sciences", 4202 };
    Rayon r3 = r1 + r2;
    r3.afficher();

    //Ex1.
    if(r3==r1)
        cout << "R3 et R1 egaux" << endl;
    if (r3!=r1)
         cout << "R3 et R1 different" << endl;

    //Ex.2
    r1();

}
