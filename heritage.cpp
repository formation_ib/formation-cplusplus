#include <iostream>



using namespace std;

class Affichable { // abstrate s'il contiens une methode pure (donc pas final)
public:
    virtual void afficher() = 0; // methode purement virtuelle
};


class Vehicule : public Affichable{
public:
    Vehicule() = default; //force l'implementation automatique de ce constructeur
    Vehicule(const Vehicule&) = delete; // empeche l'implementation automatique
    Vehicule(string n): nom(n)
    {
        //empty
    }
    virtual void afficher()
    {
        cout << nom << " ";
    }
    void afficherJoli()
    {
        cout << "#AJ# ";
        afficher();
        cout << "#AJ#";
    }
protected: // pas accesible depuis l'exterieur; accessible depuis la classe et ses descendentes
    void videNom()
    {
        nom = "";
    }
    string nom;
};

class Camionnette /*final*/ : virtual public Vehicule {
public:
    unsigned livres;
    Camionnette(string n, unsigned l): Vehicule(n), livres(l)
    {
        //empty
    }
    //void afficher() { cout << nom << " (" << livres << ") "; }
    void afficher() /*final*/ override
    {
        Vehicule::afficher();
        cout << " (" << livres << ") ";
    }
    using Vehicule::videNom; //rend public puisqu'on est dans une zone public
};


class Bus : virtual public Vehicule{
public:
    unsigned accueil;
    Bus(string n, unsigned a) : Vehicule(n), accueil(a)
    {
        //empty
    }
    void afficher() override
    {
        Vehicule::afficher();
        cout << "(max : " << accueil << ") ";

    }

};

class Bibliobus : public Camionnette, public Bus {
public:
    bool actif;
    Bibliobus(string n, unsigned l, unsigned acc, bool act) : Vehicule(n), Camionnette(n,1), Bus(n,acc), actif(act)
    {
        //empty
    }
    void afficher() override
    {
        Camionnette::afficher();
        Bus::afficher();
        if(actif)
            cout << " * actif * ";
        else
            cout << " inactif ";
    }


};


void heritageSimple() {
    cout << "Heritage : " << endl;
    Vehicule v1("Peugeot 404");
    v1.afficher();
    v1.afficherJoli();
    cout << endl;
    Camionnette c1("Citroen C4", 380);
    c1.afficher();
    // early binding : n'affiche pas le nombre de livres
    c1.afficherJoli(); // n'affiche pas le nombre de livres !
    // virtual -> late binding : affiche le nombre de livres
    c1.afficherJoli(); // virtual dans class Vehicule
    // mot cles : using --->
    c1.videNom();
    cout << endl;

    c1.afficherJoli(); // virtual dans class Vehicule
    cout << endl;

    // Affichable a1; interdit car abstraite

    Bus b1("L6", 54);
    b1.afficher();
    cout << endl;

    Bibliobus bb1("Litterature jeunesse", 420, 8, true);
    bb1.afficher();
    cout << endl;

    //Heritage multiple
//           Vehicule   Vehicule
//               \        /
//         Camionnette  Bus
//                  \   /
//                 Bibliobus

    bb1.Camionnette::afficherJoli();
    cout << endl;
    cout << sizeof(Vehicule) << ", " << sizeof(Camionnette) << ", " <<
    sizeof(Bus)<< ", " << sizeof(Bibliobus) << endl;
    cout << endl;
    //Resultat -> 28, 32, 32, 68

//////////////////////////////////////////////////////////////////////////
//Solution pour enlever le heritage multiple
//Changement the code pour y arriver:
//    class Camionnette /*final*/ : virtual public Vehicule {
//    class Bus : virtual public Vehicule{
//    Bibliobus(string n, unsigned l, unsigned acc, bool act) :
//    Vehicule(n), Camionnette(n,1), Bus(n,acc), actif(act) {}

//           Vehicule   Vehicule                 Vehicule
//                 \      /                        /  \
//         Camionnette  Bus       =>     Camionnette  Bus
//                  \   /                         \   /
//                 Bibliobus                    Bibliobus
// Resultat -> 28, 36, 36, 48


}
