#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <deque>
#include <forward_list>
#include <map>
#include <algorithm>
#include <memory>

#include "stl.h"

using namespace std;

bool assezLong(string s){return s.size()>=7;}

void collections(){
    cout << "Collections : " << endl;
    vector<string> titres {"Illiade", "Odyssee", "Le Retour"};
    titres.push_back("La vengeance");
    titres.insert(titres.begin(), "Best of Homere");
    titres[0] = "L'" + titres[0];
    cout << titres.size() << " titres" << endl;
    //Ex. Afficher tous les titres
        for(unsigned i = 0; i < titres.size(); i++){
            cout << titres[i] << endl;
        }
        cout << endl;
        for(auto it = titres.cbegin(); it !=titres.cend(); it++){
            cout << *it << " ";
        }
        cout << endl;
        for(const string& s : titres){
            cout << s << " ";
        }
        cout << endl;

    //Ex. Creer un nouveau vector a partir de titres 0, 2, 4...
    vector<string> titresPairs;
    for(auto it = titres.cbegin(); it <titres.cend(); it+=2){
        titresPairs.push_back(*it);
    }
    cout << "Titre pairs = "<<titresPairs[0] <<"-"<< titresPairs[1]<<"-" << titresPairs[2] << endl;

    vector<string> titresPairs1(titres.size()/2);
    for(auto it = titres.cbegin(); it <titres.cend(); it+=2){
        titresPairs1.push_back(*it);
    }
    cout << "Titre pairs avec limite de size = "<<titresPairs1[0] <<"-"<< titresPairs1[1]<<"-" << titresPairs1[2] << endl;

    // array : comme un vector, mais taille fixe constext: determiné a la compilation
    array<string, 3> titreCourts {"Jo", "Fame", "Si"};  // dans certains cas il sera plus rapide,
                                                        // plus petit en memoire
                                                        // plus de performance
                                                        // se souvenir de la taille
                                                        // remplir rapidement
    // list : comme un vecteur, mais sans []
    list<string> auteurs {"Homere", "Proust", "Zola"};
    // deque: file a deux bout. comme un vector avec push/pop _ front/back
    deque<long> recours{23209, 13209, 14933};
    // foward_list : version restrainte de la list car il connait que son succeseur ->
    // parcours de la fin au debut.
    forward_list<string> autresAuteurs {"Sun zu","Mac"};

    //**********Test fonction copy_if**************
    vector<string> titresLongs (titres.size());
    //copy_if(titres.begin(), titres.end(), titresLongs.begin(), assezLong);
    copy_if(titres.begin(), titres.end(), titresLongs.begin(),
        [](string s)
        {
            return s.size() >= 8;
        }
    );
    cout << "Titres Longs : " << endl;
    for(const string& s : titresLongs)
        cout << s << " ";

    //**********Test fonction any_of**************
    bool unY = any_of(titres.begin(), titres.end(),
        [](string s)
        {
            return s.find('v', 0)!=string::npos;
        }
    );
    cout << endl <<"Un Y dans l'un des titres ? " << unY << endl;

    //**********Test fonction all_of**************
    bool tousCourts = all_of(titres.begin(), titres.end(),
        [](string s)
        {
            return s.size() < 8;
        }
    );
    cout << "Tous - de 8 caracteres ? " << tousCourts << endl;

}

void ensemble() {
    map<unsigned int, unsigned> nouveautesParAnnee{{2018,23},{2019,111},{2020,34}};
    if(nouveautesParAnnee.count(2018)>0)
        cout << "2018 : " << nouveautesParAnnee[2018] << " nouveautes" << endl;
    map<unsigned, array<unsigned,12>> nouveautesParAnneeEtMoi;
    nouveautesParAnneeEtMoi[2021][3] = 0; //rien ce mois ci
}

void pointeurIntelligents() { // #include <memory>
    cout << "Smart pointers : " << endl;
    string* s1 = new string("Antenne de Borderouge");
    cout << *s1 << endl;
    delete s1;

    unique_ptr<string> s2(new string("Bibliotheque centrale"));
    cout << *s2 << endl;
    // interdit car unique : unique_ptr<string> s3 = s2;
}






























