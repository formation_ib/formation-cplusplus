#include <iostream>
#include <functional> // C++11

#include "fonctions.h"

using namespace std;

unsigned getNbLivresParRayon(unsigned longueurEnCm, unsigned etageres) {
    return longueurEnCm * etageres / 3;
}

unsigned getNbLivresParRayon(unsigned longueurEnCm, unsigned etageres,
                                unsigned largeurDunLivre) {
    return longueurEnCm * etageres / largeurDunLivre;
}

// ok / KO ?
unsigned getNbLivresParRayon(unsigned longueurEnCm) {
    return longueurEnCm/ 3;
}

unsigned getNbLivresParRayon(float longueurEnM, unsigned etageres) {
    return unsigned(longueurEnM * 100 * etageres / 3);
}

unsigned getNbLivresParRayon(double longueurEnM, unsigned etageres) {
    return unsigned(longueurEnM * 100 * etageres / 3);
}
/*
//nom d'argument : insuffisant par rapport a la premiere
unsigned getNbLivresParRayon(unsigned longueurEnM, unsigned etageres) {
    return longueurEnM * 100 * etageres / 3;
}*/

/*
//type de retour : insuffisant par rapport a la premiere
double getNbLivresParRayon(unsigned longueurEnCm, unsigned etageres) {
    return longueurEnCm * etageres / 3.0;
}*/

auto getNbLivresParRayonDbl(unsigned longueurEnCm, unsigned etageres) { //en indicant "auto" ici, il va regarder le type du "return"
    return longueurEnCm * etageres / 3.0;
}

void montreOuvrage(const string titre, const string auteur, const unsigned pages){
    cout << "**" << titre << "** de " << auteur << " (" << pages << ")" << endl;
}

void montreInfoBibli(int nbInfos, ...){
    // code de lecture
}
void parametres()
{
    cout << "Paramtres de fonctions" << endl;
    cout << "4m de long, 4 tagres : " <<
        getNbLivresParRayon(unsigned(400), 4) << " livres" << endl;
    cout << "4m de long, 4 tagres, gros livres : " <<
        getNbLivresParRayon(400, 4, 7) << " livres" << endl;

    montreOuvrage("L'Odyssee");
    montreOuvrage("L'Illiade", "Homere");
    montreOuvrage("Mes memoires", "Homere", 24);

}

void variablesFonctions(){
    //1er version
    //function < void(string, string, unsigned) > f1 = montreOuvrage;
    auto f1 = montreOuvrage;

    f1("Asterix aux Jeux Olympiques", "Uderzo, Goscinny", 44);

    //2eme version
    function < void() > f2 = bind(montreOuvrage, "Asterix le Gaulois", "Urdezo", 36);
    f2();

    //3eme version
    function < void(unsigned) > f3 = bind(montreOuvrage, "Asterix", "Urdezo", placeholders::_1);
    f3(42);

    /* Exercise:
        proposer un syntaxe pour :
        f4("Asterix et Cleapatre");
        f5(42, "La Zizanie");*/

    function < void(string) > f4 = bind(montreOuvrage, placeholders::_1, "Urdezo", 50);
    f4("Asterix et Cleapatre");
    function < void(unsigned,string) > f5 = bind(montreOuvrage, placeholders::_2, "Asterix le Gaulois", placeholders::_1);
    f5(55, "La Zizanie");


}










