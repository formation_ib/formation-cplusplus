#include <iostream>

#include "bases.h"

using namespace std;

void typesEtTestBoucle()
{
    cout << "Mediatheque de Toulouse :" << endl;
    //int ouvrages = 254000;
    // signed ourrages = 254000;
    signed int ouvrages = 254'000; // only positive numbers
    unsigned dvds = 2890; //Positive and negative numbers
//    int caisses = 9;
//    int caisses = 0b1001; // c++11
//    int caisses = 011;
//    int caisses = 9;
    int caisses = 0x9;

    cout << "Taille d'un signed int : " << sizeof(ouvrages) << "o" << endl; //4o
    char antennes = 8;
    short rayons = 460;
    int cds = 82300; //4o
    long emprunts = 123'200;
    long long entrees = 342'023;
    cout << "Tailles : " << sizeof(antennes) << "o " <<
        sizeof(rayons) << "o " <<
        sizeof(cds) << "o " <<
        sizeof(emprunts) << "o " <<
        sizeof(entrees) << "o " << endl; // 1o 2o 4o 4o 8o
        // Windows 64bits : 1o 2o 4o 4o 8o
        // Windows 64bits : 1o 2o 4o 8o 8o
    int16_t auteurs = 1219;
    int16_t max_auteurs = INT16_MAX;
    float emprunts_par_jour = 4211.8f; //32 bits. Le f es recommand� car il va creer la variable directement de 4 bytes
    double emprunts_par_ans = 322123.1; //64 bits
    double emprunts_par_ans2 = 3.22123e5; //64 bits
    bool ouvert = true;
    string nom = "Mediatheque rose"; // Chaine de caracter
    bool assez_de_rayons = (rayons > 100) && (rayons < 500);
    bool cds_anormaux = (cds > 0) || (cds < 100'000'000);
    bool cd_normaux = !cd_normaux;
    bool un_nombre_rond = (cds > 10'000) ^ (cds < 100'000);
    cds = cds +1;
    cds += 1;
    ++cds;
    cds++;

    //Exercise
    //int taille_bac = 870 / 10.999; //nombre de cd de 11mm par bac de 870cm
    double taille_bac_dbl = longueur_bac / 10.999;
    //int taille_bac = (int) taille_bac_dbl; // transtypage du C
    int taille_bac = int(taille_bac_dbl); // conversion du C++
    int bacs_cd_necessaires = (cds + taille_bac -1) / taille_bac;
    cout << "Bacs de CD de 87 cm a acheter en tout : " << bacs_cd_necessaires << endl; //1042
    int espace_restant = (taille_bac*bacs_cd_necessaires - cds); // espace restant en nombre de cds
    cout << "Espace restant dans le dernier bac : " << espace_restant << " cds " << endl;
    //Condition if
    if (espace_restant > 10){
        cout <<"On peut mettre aussi les 5 cassettes audios" << endl;
    }else{
        cout <<"On ne peut pas mettre les 5 cassettes audios" << endl;
    }

    //Condition switch
    int camion_de_bac = bacs_cd_necessaires / 260;
    switch(bacs_cd_necessaires){
    case 0:
        cout << "on demenage tout en voiture" << endl;
        break;
    case 1:
    case 2:
        cout << "un seul camion voire 2" << endl;
        break;
    default:
        cout << "Plusieurs camions" << endl;
    }
    // Exercise switch
    // a partir du nombre de CDS, afficher l'un de ceux la :
    switch(cds/50'000){
    case 0:
        cout << "Moins de 50 000 CDs" << endl;
        break;
    case 1:
    case 2:
        cout << "50 000 a 150 000 CDs" << endl;
        break;
    case 3:
    case 4:
    case 5:
        cout << "150 000 a 300 000 CDs" << endl;
        break;
    default:
        cout << "Plus de 300 00 CDs" << endl;

    }

    //Loops
    unsigned d = 0;
    while (d < bacs_cd_necessaires){
        cout << "| Bac CD n�" << d << "! |" << endl;
        d += 100;
    }

    d=0;
    do
    {
        cout << "| Bac CD n�" << d << "! #" << endl;
        d += 200;
    }while (d < bacs_cd_necessaires);

    for(d = 0; d < bacs_cd_necessaires ; d += 300){
        cout << "| Bac CD n�" << d << "! ]" << endl;
    }

    // espace_restant = 14
    for (int i = 0, j = 0; i<espace_restant ; i+=j, j++){
        cout << "Cassette : " << i << endl; //0 0 1 3 6 10
    }

    //Usage de types pour le tablaux
//    float usagers_par jour[7]; // ok
//    float usagers_par jour[3+2*2]; // ok
    int jours = 7;
    float usagers_par_jour[jours]; // KO because it cannot include variables
//    const int JOURS = 7;
//    float usagers_par jour[JOURS]; // ok
//    float usagers_par jour[JOURS*2-7]; // ok
    usagers_par_jour[0] = 125.98f;
    //usagers_par_jour[7] = 90; // In this line we can found a crash
    //cout << "usagers_par_jours : " << usagers_par_jour[7] << endl;

    float usagers_weekend [] { 320.5f, 0 };
    float usagers_par_demi_jour[7][2]; // 56 bytes
    usagers_par_demi_jour[6][1] = 0; // dimanche apr�s

    //Iteration loop to look inside a table
    for(float uj : usagers_par_jour) // c++11
        cout << uj << " ";

    //Exercise
    // Classification Dewey (1 : philosophie ... 9 : histoire)
    unsigned livres_par_rayon []{2300, 1432, 2394, 22333, 23209, 9893, 10923, 9288,1420 };
    unsigned plus_petit_rayon = 0, plus_grand_rayon = 0;
    for (unsigned i = 1; i < 9; i++){
        if(livres_par_rayon[i] < livres_par_rayon[plus_petit_rayon])
            plus_petit_rayon = i;
        if(livres_par_rayon[i] > livres_par_rayon[plus_grand_rayon])
            plus_grand_rayon = i;

    }

    cout << "Rayon avec le moins de livres : " << (plus_petit_rayon+1) << endl;
    cout << "Rayon avec le plus de livres : " << (plus_grand_rayon+1) << endl;



}
