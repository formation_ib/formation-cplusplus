#include <iostream>

#include "objets.h"

using namespace std;


struct Usager{
    string prenom, nom;
    unsigned age;
    void afficher(){
        cout << prenom << " " << nom << " a " << age << " ans" << endl;
    }
    void vieillir(unsigned plusvieux){
        age = age + plusvieux;
    }
    void rajeunir(unsigned plusjeune){
        vieillir(-plusjeune);
    }

};


void basesDeLobjet(){
    cout << " Classes et objets " << endl;

    /*1ere option*/
    Usager u1;
    u1.prenom = "Luis";
    u1.nom = "Caro";
    u1.age = 86;
    cout << "Prenom : " << u1.prenom << endl;
    cout << "Nom : " << u1.nom << endl;
    cout << "Age : " << u1.age << endl;
    u1.vieillir(4);
    u1.rajeunir(2);
    u1.afficher();

    /*2eme option*/
    Usager * u2 = new Usager;
    //(*u2).prenom = "Pedrito";
    u2->prenom = "Pedrito";
    //(*u2).nom = "Pedrito";
    u2->nom = "Zacks";
    //(*u2).age = "Pedrito";
    u2->age = 60;
    //cout << u2->prenom << " " << u2->nom << " a " << u2->age << " ans" << endl;
    u2->afficher();
    delete u2;
}

//*******Implementation d'une metode d'une class**********
void Employe::afficher(){
    cout << this->nom << " (" << id << ")" << endl;
}

//***********Implementation du constructeur**************
    //#1
//    Employe::Employe() : nom(""), id(1000){ //nom = "";//id = 1000;
//    }

    //#2
//    Employe::Employe(string n) : nom(n), id(1000){
//    }
        //2eme option
        //Employe::Employe(string nom) : nom(nom), id(1000){
        //}

    //#3
//    Employe::Employe(string n, unsigned i) : nom(n), id(i){
//    }

    //#4 : Constructeurs appelle un autre constructeur
    //Employe::Employe(string n) :  Employe () {nom = n;}

//Ex. Utiliser le cas #4 pour appeller les constructeurs sur constructeur
Employe::Employe() : Employe("") {}
Employe::Employe(string n) :  Employe (n, 1000) {}
Employe::Employe(string n, int i) : nom(n), id (i){}

//destructeurs : il est appell� 3 fois
Employe::~Employe() { cout << "del~ " << endl; }


void objetsComplets(){
    //Ex. Tester la classe Employe


    //old syntaxe : avec de parentheses
    //int n1 = 3;
    // int n2(3);
    //interdit : declaration de fonction : Employeu1();
//    Employe u1;//ressemble a une methode plutot qu'un objet
//    u1.afficher();
//    Employe u2("Carol");
//    u2. afficher();
//    Employe u3("Dana", 1018);
//    u3.afficher();


    //new syntaxe : il faudra mettre tout en accollades
    //si je vais modifier les object -> ex : int n3 {3}
    Employe u1 {};//ressemble a une methode plutot qu'un objet
    u1.afficher ();
    Employe u2 {"Carol"};
    u2. afficher();
    Employe u3{"Dana", 1018};
    u3.afficher();

}

Equipe::Equipe() : Equipe(0) {}
Equipe::Equipe(unsigned t) : taille(t), employes(new Employe[t]) {}
Equipe::Equipe(const Equipe& src) : taille(src.taille), employes(new Employe[src.taille]) {
    for(unsigned i = 0; i < taille ; i++)
    {
        this->employes[i] = src.employes[i];
    }
}
//Constructeur par deplacement
Equipe::Equipe(Equipe&& scr) : taille(scr.taille), employes(scr.employes){
    scr.employes = nullptr;
}

Equipe::~Equipe() {delete [] employes;}

void Equipe::afficher(){
    for(unsigned i= 0; i < taille ; i++)
    {
        cout << " - ";
        employes[i].afficher();
    }
}

Equipe& Equipe::operator =(const Equipe& src){
    if(this != &src) { //rien faire si eq1 = eq1
        this->taille = src.taille;
        delete [] this->employes;
        this->employes = new Employe[src.taille];
        for(unsigned i = 0; i<taille; i++)
            this->employes[i] = src.employes[i];
    }
    return *this;
}

//Operateur par deplacement
Equipe& Equipe::operator =(Equipe&& src){
    if(this != &src) { //rien faire si eq1 = eq1
        this->taille = src.taille;
        this->employes = src.employes;
        src.employes = nullptr;
    }
    return *this;
}

// Creation d' Objet temporaire
Equipe getLequipeAMoi(){
    Equipe eq {1};
    eq.employes[0].nom = "Moi";
    eq.employes[0].id = 2000;
    return eq;
}

void formeCanonique(){
    Equipe eq1 {3};
    //
    eq1.employes[0] = Employe("Tom", 1092);
    eq1.employes[1] = Employe("Beate", 1093);
    //eq1.employes[2] = Employe("Klinz", 1098);
    // plus economie :
    eq1.employes[2].nom = "Klinz";
    eq1.employes[2].id = 1098;
    eq1.afficher();

    //constructeur par recopie
    Equipe eq2 { eq1 };
    Equipe eq3;
    //Constructeur par affectation, operateur =
    eq3 = eq1;

    Equipe eq4 = eq1; // ---> constructeur par recopie
    eq1.employes[0].id = 4001; // ne change pas eq4
    eq4.afficher();

    //Constructeur par deplacement
    Equipe eq5 = getLequipeAMoi(); //La meme chose -> Equipe eq5 = (etLequipeAMoi());
    eq5.afficher();

    //Operateur par deplacement ; "objet egal un objet temporaire"
    eq5 = getLequipeAMoi();
}











