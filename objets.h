#ifndef OBJETS_H_INCLUDED
#define OBJETS_H_INCLUDED

//using namespace std;

void basesDeLobjet();
void objetsComplets();
void formeCanonique();

struct Employe{
    std::string nom = "lol";
    int id;

    Employe();       // #1
    Employe(std::string); // #2
    Employe(std::string, int); // #3
    ~Employe();
    void afficher();
};

// Canonique :
// - ctor
// - dtor
// - ctor(&scr) (recopie)
// - op=(&scr) (affectation)
struct Equipe{
    Employe employes1[10];//pas tres convenient alors on utilise de pointers --->>
    Employe * employes; //Tableau d'objet
    unsigned taille;

    Equipe();
    Equipe(unsigned);
    Equipe(const Equipe&);// - ctor(&scr) (recopie)
    Equipe(Equipe&&);
    Equipe& operator = (const Equipe&); // - op=(&scr) (affectation)
    Equipe& operator = (Equipe&&); // - op=(&scr) (affectation)
    ~Equipe();
    void afficher();
};

#endif // OBJETS_H_INCLUDED
