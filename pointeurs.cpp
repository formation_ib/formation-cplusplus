#include <iostream>

#include "pointeurs.h"

using namespace std;

void pointeursEtTableaux(){

    //**********POITERS*************
    cout << "Pointeurs : " << endl;
    unsigned a = 27; // 4 bytes
    unsigned * pa = &a; // address in RAM memory
    cout << "a veut" << a << ", est a l'adresse :" << pa << endl;
    cout << "pa est a l'address :" << &pa << ", proche de a" << endl;
    unsigned ** ppa = &pa; // a pointer over another one
    cout << "ou en passant par une nouvelle variable : " << ppa << endl;

    cout << "a, pa et ppa : " << a << " " << pa << " " << ppa << endl;
    a++;
    cout << "a, pa et ppa : " << a << " " << pa << " " << ppa << endl; //The only one that is modified is "a"

    //forbidden : pa = & 4203;
    //forbidden : pa

    cout << "Ce qu'il y a a l'adresse pa : " << *pa << endl;// *pa will show what is in the address
    //cout << "Ce qu'il y a a l'adresse pa : " << *a << endl;// *a is not an address

    //**********TABLEAUX POINTERS*************
    float tf[5] {0, 2, 4, 6, 8};
    float * pf = nullptr;
    if(pf != nullptr)
        cout << "Valeur derriere pf : " << *pf << endl;

    //equivaut a: pf = &tf;
    //equivaut a: pf = &tf[0];
//    pf = tf;
//    cout << "Le 4eme valeur du tableau : " << tf[3] << endl;
//    cout << "Le 4eme valeur du tableau : " << *(pf+3) << endl;
//
//    pf = pf + 3;
//    cout << "Le 4eme valeur du tableau : " << *pf << endl;

    //Exercise
    //Afficher la somme des valeurs du tableau sans utiliser de []
    float somme = 0;
    for (pf = tf ; pf < tf+5; pf++)
        somme += *(pf);

    cout << "La somme des valeurs du tableau est: " << somme << endl;
}

    //**********TABLEAUX ET FONCTIONS*************

// fonction qui recoit un tableau :
unsigned minAnnee(unsigned * annees, unsigned taille){
    unsigned min = 2100;
    // interdit car taille inconnue : for (unsigned a : annees)
    for (unsigned i = 0; i < taille; i++)
        if (annees [i] < min)
            min = annees[i];
    return min;
}

// fonction qui renvoit un tableau :
//unsigned* anciennnetes(unsigned * annees, const unsigned taille){
//    unsigned res[taille];
//    // interdit car taille inconnue : for (unsigned a : annees)
//    for (unsigned i = 0; i < taille; i++)
//        res[i] = 2021 - annees[i];
//    return res;
//}// cuando sale de esta funcion, "res" pierde sus valores ; quiere decir que los valores que apunta la direccion se pierden

//example avec PILE : crash
//unsigned* anciennnetesPile(unsigned * annees, const unsigned taille){
//    unsigned res[taille]; // pas const expr !?
//    for (unsigned i = 0; i < taille; i++)
//        res[i] = 2021 - annees[i];
//    return res;
//}

//Solution avec STATIC
unsigned* anciennnetesStatique(unsigned * annees){
    static unsigned res[6];
    // interdit car taille inconnue : for (unsigned a : annees)
    for (unsigned i = 0; i < 6; i++)
        res[i] = 2021 - annees[i];
    return res;
}

//Solution avec TAS : toca suprimir los elementos del TAS
unsigned* anciennnetesTas(unsigned * annees, const unsigned taille){
    unsigned *res = new unsigned[taille]; // Se agrega una nueva direccion dentro de la Pila
    // interdit car taille inconnue : for (unsigned a : annees)
    for (unsigned i = 0; i < 6; i++)
        res[i] = 2021 - annees[i];
    return res;
}

void tableauxEtFonctions(){
    cout << "Tableaux et fonctions " << endl;
    unsigned antennes[] { 1967, 1966, 1982, 2002, 1987, 2004};
    cout << "Ouverture : " << minAnnee(antennes, 6) << endl;
//    unsigned * ans = anciennnetes(antennes, 6);  // crash
//    unsigned * ans = anciennnetesPile(antennes, 6);  // Pile : crash
    unsigned * ansStatique = anciennnetesStatique(antennes);  // Static
    for (unsigned i = 0; i < 6; i++)
        cout << ansStatique[i] << " ";
    cout << endl;
    unsigned * ansTas = anciennnetesTas(antennes,6);  // Tas
    for (unsigned i = 0; i < 6; i++)
        cout << ansTas[i] << " ";
    delete[] ansTas;
}



    //**********REFERENCES*************
//It is used mostly when I am using big quantity of data
//Mask pointers that avoid Catastrophics
// TIP: use references instead of POINTERS
void afficheBluerays(unsigned & new_bluerays){
    new_bluerays = 10 * unsigned(new_bluerays / 10);
    cout << "** Bluerays : " << new_bluerays << endl;
}

unsigned& afficheBlueraysA25Pret(unsigned & new_bluerays){  // (unsigned new_bluerays) without the "&" reference to temporary element will crash
    new_bluerays = 25 * unsigned(new_bluerays / 25);
    return new_bluerays;
}

//!§References a droite
unsigned getHeureOuverture() { return 9 ;}

string mettreEnForme(const string & s) {
    string resultat = " ---- " + s + " ---- ";
    return resultat;
//    s = " **** " + s + " **** ";
//    return s;
}

string mettreEnForme(string && s) {
    s = " **** " + s + " **** ";
    return s;
}

void references(){
    unsigned bluerays = 3245;
    unsigned& br2 = bluerays; // br2 est un allias de bluerays
    bluerays += 481;
    cout << "Bluerays : " << br2 << endl; //3726
    afficheBluerays(bluerays);
    cout << "Bluerays : " << br2 << endl; //3720
    // afficheBluerays(10000);          //interdit car on peut pas modifier 10000
    // afficheBluerays(bluerays + 5);   //interdit car on peut pas modifier 5
    // FORBIDDEN --->>
    // unsigned & br3; // This line can be empty or NULL
    // br3 = bluerays;
    unsigned& br4 = afficheBlueraysA25Pret(bluerays);
    cout << "Bluerays (~25) " << br4 << endl; //3700
    cout << "Bluerays (~25) " << bluerays << endl; //3700

    //!§ References a droite
    unsigned h = getHeureOuverture();       //il y a un copy de l'objet dans la veritable variable h --->> LOW PERFORMANCE
    //unsigned & h2 = getHeureOuverture();  //interdit car rvalue dans ref unsigned...
    unsigned && h2 = getHeureOuverture();   //il y a une reference que pour h2 et pas de copy --->> HIGH PERFORMANCE

    string titre = "C++ avance (2eme edition)";
    string misEnForme = mettreEnForme(titre);
    cout << titre << " => " << misEnForme << endl;
    cout << mettreEnForme("La programmation c'est facile") << endl;

}




